﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FloorManager : MonoBehaviour
{
	readonly float leftBorder=-3;//左邊界
	readonly float rightBorder=3;//右邊界
	readonly float initPositionY=0;
	[Range(2,6)] public float spacingY;
	[Range(1,20)]public float singleFloorHeight;
	readonly int Max_Ground_Count=10;//最大地板數量
	readonly int Min_Ground_Count_Under_Player=3; //玩家下方最少地板數量
	static int groundNumber=-1;
	public List<Transform> grounds;

	public Text displayCountFloor;
	public Transform player;
    // Start is called before the first frame update
    void Start()
    {
		grounds= new List<Transform>();
		for(int i=0;i<Max_Ground_Count;i++)
		{
			SpawnGround();
		}
    }
	float newGroundPositionX()
	{
		if(grounds.Count==0)
		{
			return 0;
		}
		return Random.Range(leftBorder,rightBorder);
	}
	float newGroundPositionY()
	{
		if(grounds.Count==0)
		{
			return initPositionY;
		}
		int lowerIndex=grounds.Count-1;
		return grounds[lowerIndex].transform.position.y-spacingY;
	}
    // Update is called once per frame
    void Update()
    {
		ControlSpawnGround();
		DisplayCountFloor();
    }
	void SpawnGround()
	{
		GameObject newGround=Instantiate(Resources.Load<GameObject>("Floor"));
		newGround.transform.position=new Vector3(newGroundPositionX(),newGroundPositionY(),0);
		grounds.Add(newGround.transform);
	}
	public void ControlSpawnGround()//控制產生地板
	{
		int groundsCountUnderPlayer=0;//玩家下方地板數量

		foreach(Transform ground in grounds)  //檢查玩家下方地板
		{
			if(ground.position.y<player.transform.position.y)
			{
				groundsCountUnderPlayer++;
			}
		}
		if(groundsCountUnderPlayer<Min_Ground_Count_Under_Player)//小於一定數量產生地板
		{
		SpawnGround();
		ControlGroundsCount();
		}
	}
	void ControlGroundsCount()//控制地板數量
	{
		if(grounds.Count>Max_Ground_Count)
		{
			Destroy(grounds[0].gameObject);
			grounds.RemoveAt(0);
		}
	}
	float CountLowerGroundFloor()
	{
		float playerPositionY=player.transform.position.y;
		float deep=Mathf.Abs(initPositionY-playerPositionY);
		return (deep/singleFloorHeight)+1;
	}
	void DisplayCountFloor()
	{
		displayCountFloor.text="地下"+CountLowerGroundFloor().ToString("0000")+"樓";
	}
}
