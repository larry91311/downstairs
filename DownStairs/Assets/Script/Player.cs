﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public float forceX; //水平推力
	Rigidbody2D playerRigidBody2D;
	readonly float toLeft=-1;
	readonly float toRight=1;
	readonly float stop=0;
	float directionX;

	public static bool isDead;
    // Start is called before the first frame update
    void Start()
    {
		isDead=false;
		playerRigidBody2D=GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
		if(Input.GetKey(KeyCode.LeftArrow))    //鍵入確定力的方向
		{
			directionX=toLeft;
		}
		else if(Input.GetKey(KeyCode.RightArrow))
		{
			directionX=toRight;
		}
		else
		{
			directionX=stop;
		}

		Vector2 newDirection=new Vector2(directionX,0);   //定義確定力方向後的位置
		playerRigidBody2D.AddForce(newDirection*forceX);    //朝位置施力
    }
}
