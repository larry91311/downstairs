﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
	public float downSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
	//預設每秒執行50次
    void FixedUpdate()
    {
		transform.Translate(0,-downSpeed*Time.deltaTime,0);
    }
}
